#include "pentagono.hpp"
#include <iostream>

Pentagono::Pentagono(string tipo, float base, float apotema){
    set_tipo(tipo);
    set_base(base);
    this -> apotema = apotema;
}
int Pentagono::calcula_area(float base, float apotema){
    return ((5 * base) * apotema) / 2;
}
int Pentagono::calcula_perimetro(float base){
    return 5 * base;
}
