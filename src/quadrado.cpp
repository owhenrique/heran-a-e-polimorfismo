#include "quadrado.hpp"
#include <iostream>

Quadrado::Quadrado(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}
int Quadrado::calcula_area(float base){
    return base*base;
}
int Quadrado::calcula_perimetro(float base){
    return 4*base;
}
