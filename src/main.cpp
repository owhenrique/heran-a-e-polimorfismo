#include "triangulo.hpp"
#include "quadrado.hpp"
#include "circulo.hpp"
#include "paralelograma.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"

#include <iostream>
#include <string>
#include <vector>

using namespace std;


int main(int argc, char ** argv){

    vector <FormaGeometrica* > formas;

    int contador = 0;

    cout << "Exercício de Herança e Polimorfismo! :D" << endl;

    formas.push_back(new Triangulo("Triangulo", 4.0, 4.0));
    formas.push_back(new Quadrado("Quadrado, ", 4.0, 4.0));
    formas.push_back(new Circulo("Circulo", 1.0, 6.28));
    formas.push_back(new Paralelograma("Quadrado", 6.0, 10.0));
    formas.push_back(new Pentagono("Pentagono", 3.0, 2.0));
    formas.push_back(new Hexagono("Hexagono", 9.0));
    
    cout << "------------------------------------------------------------" << endl;

    while(contador <= 5){
        cout << "Tipo: " << formas.at(contador)->get_tipo() << endl;
        cout << "Área: " << formas.at(contador)->calcula_area() << endl;
        cout << "Perímetro: " << formas.at(contador)->calcula_perimetro() << endl;
        formas.at(contador)->~FormaGeometrica();
        cout << "------------------------------------------------------------" << endl;
        contador++;
    }

    return 0;

}