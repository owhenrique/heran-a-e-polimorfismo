#include "paralelograma.hpp"
#include <iostream>

Paralelograma::Paralelograma(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}
int Paralelograma::calcula_area(string tipo, float base, float altura){
    if(tipo == "Quadrado")
        return base * base;
    if(tipo == "Losango")
        return (base * altura) / 2;
    if(tipo == "Retângulo")
        return base * altura;
    
}
int Paralelograma::calcula_perimetro(string tipo, float base, float altura){
    if(tipo == "Quadrado")
        return 4 * base;
    if(tipo == "Losango")
        return 4* base;
    if(tipo == "Retângulo")
        return (2 * base) + (2 * altura);
    else
        throw(1);
}
 