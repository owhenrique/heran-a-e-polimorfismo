#include "circulo.hpp"
#include <iostream>

Circulo::Circulo(string tipo, float raio, float circunferencia){
    set_tipo(tipo);
    set_raio(raio);
    set_circunferencia(circunferencia);
}
void Circulo::set_raio(float raio){
    if(raio < 0)
        throw(1);
    else
        this -> raio = raio;
}
float Circulo::get_raio(){
    return raio;
}
void Circulo::set_circunferencia(float circunferencia){
    if(circunferencia < 0)
        throw(1);
    else
        this -> circunferencia = circunferencia;
}
float Circulo::get_circunferencia(){
    return circunferencia;
}
int Circulo::calcula_area(float raio){
    return 3.14159 * (raio * raio);
}

int Circulo::calcula_perimetro(float raio){
    return 2* 3.14159 * raio;
}