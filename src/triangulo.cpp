#include "triangulo.hpp"
#include <iostream>

Triangulo::Triangulo(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);    
}
int Triangulo::calcula_area(float base, float altura){
    return (base * altura) / 2;
}
int Triangulo::calcula_perimetro(float base, float altura, float hip){
    return base + altura + hip;
}