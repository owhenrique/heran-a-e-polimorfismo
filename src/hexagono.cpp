#include "hexagono.hpp"
#include <iostream>

Hexagono::Hexagono(string tipo, float base){
    set_tipo(tipo);      
    set_base(base);
}
int Hexagono::calcula_area(float base){
    return 2.598 * (base * base);
}
int Hexagono::calcula_perimetro(float base){
    return 6 * base;
}