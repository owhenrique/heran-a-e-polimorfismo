#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Pentagono: public FormaGeometrica {
    private:
        float apotema;
    public:
        Pentagono(string tipo, float base, float apotema);
        ~Pentagono();
        int calcula_area(float base, float apotema);
        int calcula_perimetro(float base);
};


#endif