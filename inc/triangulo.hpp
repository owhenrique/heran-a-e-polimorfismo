#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Triangulo: public FormaGeometrica {
    float hip;
    public:
        Triangulo(string tipo, float base, float altura);
        ~Triangulo();
        int calcula_area(float base, float altura);
        int calcula_perimetro(float base, float altura, float hip);    
    
};

#endif