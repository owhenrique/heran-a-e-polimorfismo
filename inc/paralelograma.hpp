#ifndef PARALELOGRAMA_HPP
#define PARALELOGRAMA_HPP

#include <string>
#include "formageometrica.hpp"

class Paralelograma: public FormaGeometrica {
    public:
        Paralelograma(string tipo, float base, float altura);
        ~Paralelograma();
        int calcula_area(string tipo, float base, float altura);
        int calcula_perimetro(string tipo, float base, float altura);
};

#endif