#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Quadrado: public FormaGeometrica {
    public:
        Quadrado(string tipo, float base, float altura);
        ~Quadrado();
        int calcula_area(float base);
        int calcula_perimetro(float base);
};

#endif