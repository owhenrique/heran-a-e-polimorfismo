#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include <string>
#include "formageometrica.hpp"

class Circulo: public FormaGeometrica {
    private:
        float raio;
        float circunferencia;
    public:
        Circulo(string tipo, float raio, float circunferencia);
        ~Circulo();
        void set_raio(float raio);
        float get_raio();
        void set_circunferencia(float circunferencia);
        float get_circunferencia();
        int calcula_area(float raio);
        int calcula_perimetro(float raio);
};

#endif