#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Hexagono: public FormaGeometrica {
    public:
        Hexagono(string tipo, float base);
        ~Hexagono();
        int calcula_area(float base);
        int calcula_perimetro(float base);
};

#endif 